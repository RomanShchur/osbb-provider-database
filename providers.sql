CREATE TABLE IF NOT EXISTS `mydb`.`provider` (
  `company_code` INT(11) NOT NULL,
  `company_name` VARCHAR(100) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `comment` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`company_code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mydb`.`contacts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `contact_person` VARCHAR(70) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `privider_company_code` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `privider_company_code`),
  INDEX `fk_contacts_privider_idx` (`privider_company_code` ASC),
  CONSTRAINT `fk_contacts_privider`
    FOREIGN KEY (`privider_company_code`)
    REFERENCES `mydb`.`privider` (`company_code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mydb`.`contract` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `contract_id` VARCHAR(15) NOT NULL,
  `date` DATE NOT NULL,
  `privider_company_code` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_contract_privider1_idx` (`privider_company_code` ASC),
  CONSTRAINT `fk_contract_privider1`
    FOREIGN KEY (`privider_company_code`)
    REFERENCES `mydb`.`privider` (`company_code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- CREATE TABLE IF NOT EXISTS `mydb`.`osbb(Figural)` (
--   `idosbb` INT(11) NOT NULL AUTO_INCREMENT,
--   PRIMARY KEY (`idosbb`))
-- ENGINE = InnoDB
-- DEFAULT CHARACTER SET = utf8;

--================== HERE COULD BE CONNECTION TO OSBB TABLE ===========--
CREATE TABLE IF NOT EXISTS `mydb`.`osbb_has_provider` (
  -- here
  `osbb_idosbb` INT(11) NOT NULL,
  `provider_company_code` INT(11) NOT NULL,
  PRIMARY KEY (`osbb_idosbb`, `privider_company_code`),
  INDEX `fk_osbb(Figural)_has_privider_privider1_idx` (`privider_company_code` ASC),
  INDEX `fk_osbb(Figural)_has_privider_osbb(Figural)1_idx` (`osbb_idosbb` ASC),
  CONSTRAINT `fk_osbb(Figural)_has_privider_osbb(Figural)1`
    FOREIGN KEY (`osbb_idosbb`)
    REFERENCES `mydb`.`osbb(Figural)` (`idosbb`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_osbb(Figural)_has_privider_privider1`
    FOREIGN KEY (`privider_company_code`)
    REFERENCES `mydb`.`privider` (`company_code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
